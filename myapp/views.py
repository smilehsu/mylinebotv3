from django.conf import settings
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseForbidden
from django.views.decorators.csrf import csrf_exempt

from linebot import LineBotApi, WebhookParser
from linebot.exceptions import InvalidSignatureError, LineBotApiError
from linebot.models import MessageEvent, TextSendMessage
from module import func


line_bot_api = LineBotApi(settings.LINE_CHANNEL_ACCESS_TOKEN)
parser = WebhookParser(settings.LINE_CHANNEL_SECRET)



#傳什麼訊息回覆相同訊息的範例
@csrf_exempt
def callback(request):
    if request.method == 'POST':
        signature = request.META['HTTP_X_LINE_SIGNATURE']
        body = request.body.decode('utf-8')

        try:
            events = parser.parse(body, signature)
        except InvalidSignatureError:
            return HttpResponseForbidden()
        except LineBotApiError:
            return HttpResponseBadRequest()

        #這次修改的程式碼 開始
        for event in events:
            if isinstance(event, MessageEvent):
                #mtext = 使用者輸入的字串
                mtext = event.message.text
                if mtext == '@help':
                    func.sendUse(event)
                else:
                    func.sendScrachStock(event, mtext)
        #這次修改的程式碼 結束

        return HttpResponse()
    else:
        return HttpResponseBadRequest()